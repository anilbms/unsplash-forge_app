// Import required components from Forge UI library
import ForgeUI, {
  render,
  Text,
  Button,
  Fragment,
  Image,
  useAction
} from "@forge/ui";
import API from "@forge/api";
import Unsplash, { toJson } from "unsplash-js";

const unsplash = new Unsplash({
  accessKey: process.env.UNSPLASH_ACCESS_KEY
});

// Unsplash interface to be used by our getRandomImage function
interface UnsplashRandomImageJson {
  title: string;
  url: string;
}

// ImageCardProps interface which will be used by ImageCard component
interface ImageCardProps {
  title: string;
  src: string;
}

// getRandomUnsplashImage function makes the UNSPLASH API call to get a random image and filter out title and url
const getRandomUnsplashImage = async (): Promise<UnsplashRandomImageJson> => {
  const response = await unsplash.photos.getRandomPhoto();
  const {
    description: title,
    urls: { thumb: url }
  } = await response.json();

  return {
    title,
    url
  };
};

// ImageCard component containing text and image
const ImageCard = ({ title, src }: ImageCardProps) => (
  <Fragment>
    <Text content={title} />
    <Image src={src} alt={title} />
  </Fragment>
);

// App function will return the final output
const App = () => {
  const globalAny: any = global; // Typescript hack to attach property to global
  globalAny.fetch = API.fetch; // Assign API.fetch to global fetch

  const [{ title, url }, setRandomUnsplashImage] = useAction(
    getRandomUnsplashImage,
    getRandomUnsplashImage
  );

  return (
    <Fragment>
      <Text content="Random Image!" />
      <ImageCard src={url} title={title} />
      <Button
        text="⏭️ GET RANDOM IMAGE"
        onClick={() => {
          setRandomUnsplashImage();
        }}
      />
    </Fragment>
  );
};
